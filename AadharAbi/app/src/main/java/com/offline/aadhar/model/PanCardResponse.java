package com.offline.aadhar.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class PanCardResponse {

    @Expose
    @SerializedName("status")
    private String status;
    @Expose
    @SerializedName("token")
    private int token;
    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("PAN")
    private PAN PAN;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public int getToken() {
        return token;
    }

    public void setToken(int token) {
        this.token = token;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public PAN getPAN() {
        return PAN;
    }

    public void setPAN(PAN PAN) {
        this.PAN = PAN;
    }

    public static class PAN {
        @Expose
        @SerializedName("first_name")
        private String first_name;
        @Expose
        @SerializedName("title")
        private String title;
        @Expose
        @SerializedName("middle_name")
        private String middle_name;
        @Expose
        @SerializedName("pan_status")
        private String pan_status;
        @Expose
        @SerializedName("pan_no")
        private String pan_no;
        @Expose
        @SerializedName("last_name")
        private String last_name;
        @Expose
        @SerializedName("last_update")
        private String last_update;

        public String getFirst_name() {
            return first_name;
        }

        public void setFirst_name(String first_name) {
            this.first_name = first_name;
        }

        public String getTitle() {
            return title;
        }

        public void setTitle(String title) {
            this.title = title;
        }

        public String getMiddle_name() {
            return middle_name;
        }

        public void setMiddle_name(String middle_name) {
            this.middle_name = middle_name;
        }

        public String getPan_status() {
            return pan_status;
        }

        public void setPan_status(String pan_status) {
            this.pan_status = pan_status;
        }

        public String getPan_no() {
            return pan_no;
        }

        public void setPan_no(String pan_no) {
            this.pan_no = pan_no;
        }

        public String getLast_name() {
            return last_name;
        }

        public void setLast_name(String last_name) {
            this.last_name = last_name;
        }

        public String getLast_update() {
            return last_update;
        }

        public void setLast_update(String last_update) {
            this.last_update = last_update;
        }
    }
}
