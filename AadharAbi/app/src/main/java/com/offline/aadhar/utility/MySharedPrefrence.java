package com.offline.aadhar.utility;

import android.content.Context;
import android.preference.PreferenceManager;

import com.bumptech.glide.ListPreloader;

public class MySharedPrefrence  {

    public static void setValueInSession(Context context,String key, String value){
        PreferenceManager.getDefaultSharedPreferences(context).edit().putString(key,value).apply();
    }

    public static String getValueFromSession(Context context,String key){
        return PreferenceManager.getDefaultSharedPreferences(context).getString(key,"");
    }
}
