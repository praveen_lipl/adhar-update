package com.offline.aadhar;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.offline.aadhar.Retrofit.ApiClient;
import com.offline.aadhar.Retrofit.ApiInterface;
import com.offline.aadhar.model.PanCardRequest;
import com.offline.aadhar.model.PanCardResponse;
import com.offline.aadhar.utility.MySharedPrefrence;


import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.offline.aadhar.Retrofit.ApiClient.URL_KEY;
import static com.offline.aadhar.camera.Util.generateToken;
import static com.offline.aadhar.utility.Utils.hideKeyboard;
import static com.offline.aadhar.utility.Utils.isNetworkConnected;
import static com.offline.aadhar.utility.Utils.verifyPan;


public class PanCardVerificationAcctivity extends AppCompatActivity {

    @BindView(R.id.inputPANcardNo)
    EditText inputPANcardNo;
    @BindView(R.id.submit)
    Button submit;
    @BindView(R.id.inputPancardLayout)
    LinearLayout inputPancardLayout;
    @BindView(R.id.userName)
    TextView userName;
    @BindView(R.id.userpanNo)
    TextView userpanNo;
    @BindView(R.id.userUpdatedOn)
    TextView userUpdatedOn;
    @BindView(R.id.detailPanCardLayout)
    LinearLayout detailPanCardLayout;

    @BindView(R.id.iconLayout)
    LinearLayout iconLayout;

    @BindView(R.id.actionLayout)
    LinearLayout actionLayout;
    @BindView(R.id.close)
    Button close;

    @BindView(R.id.done)
    Button done;
    int counter = 0;
    @BindView(R.id.tryAgain)
    Button tryAgain;
    @BindView(R.id.success)
    ImageView success;
    @BindView(R.id.failed)
    ImageView failed;
    @BindView(R.id.panStatus)
    TextView panStatus;
    @BindView(R.id.panStatusLayout)
    LinearLayout panStatusLayout;

    private ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pancard_verification);
        ButterKnife.bind(this);


    }
    @OnClick({R.id.submit, R.id.tryAgain, R.id.success, R.id.close, R.id.done})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.submit:
                hideKeyboard(this);
                if (!TextUtils.isEmpty(inputPANcardNo.getText().toString())) {
                    if (verifyPan(inputPANcardNo.getText().toString()))
                        if (isNetworkConnected(this))
                            callApi();
                        else Toast.makeText(PanCardVerificationAcctivity.this, "Please check Internet connection", Toast.LENGTH_SHORT).show();
                    else
                        inputPANcardNo.setError("Invalid PAN no.");

                } else {
                    Toast.makeText(PanCardVerificationAcctivity.this, "Please enter PAN number", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.tryAgain:
                if (isNetworkConnected(this))
                    if (!MySharedPrefrence.getValueFromSession(this,URL_KEY).equalsIgnoreCase(""))
                    callApi();
                else Toast.makeText(PanCardVerificationAcctivity.this, "Base URL not found", Toast.LENGTH_SHORT).show();
                else Toast.makeText(PanCardVerificationAcctivity.this, "Please check Internet connection", Toast.LENGTH_SHORT).show();

                break;
            case R.id.close:
                finish();
                break;
            case R.id.done:
                finish();
                break;

        }
    }

    private void callApi() {
        int UserToken = generateToken();
        initializeProgress();
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        PanCardRequest panRequest = new PanCardRequest();
        panRequest.setPan(inputPANcardNo.getText().toString());
        panRequest.setToken(UserToken);
        Call<PanCardResponse> call = apiService.sendPanCardNumber(MySharedPrefrence.getValueFromSession(getApplicationContext(),URL_KEY)+"panws/panwsapi/panval",panRequest);
        Log.e("PAN URL",MySharedPrefrence.getValueFromSession(getApplicationContext(),URL_KEY)+"panws/panwsapi/panval");

        call.enqueue(new Callback<PanCardResponse>() {
            @Override
            public void onResponse(Call<PanCardResponse> call, Response<PanCardResponse> response){

                progressDialog.dismiss();
                Log.e("pancard result", response.body().toString());

                if (response.isSuccessful()) {
                    inputPANcardNo.setText("");
                    inputPancardLayout.setVisibility(View.GONE);
                    detailPanCardLayout.setVisibility(View.VISIBLE);
                    PanCardResponse results = response.body();

                    String test = "test";
                    if (results.getPAN() != null) {
                        switch (results.getPAN().getPan_status()) {
                            //
                            case "D":
                                displayMessageWithCross(results.getMessage());
                                System.out.println("Deleted");
                                break;
                            case "N":
                                displayMessageWithCross(results.getMessage());
                                System.out.println("Not present in Income Tax Department (ITD) database / Invalid PAN");
                                break;
                            case "F":
                                displayMessageWithCross(results.getMessage());
                                System.out.println("Marked as Fake");
                                break;
                            case "X":
                                displayMessageWithCross(results.getMessage());
                                System.out.println("Marked as DeactivatedThese will be cross and rest will have tick");
                                break;


                            //display pan details
                            case "EU":
                                displayMessageWithDetails(results.getMessage(),results);
                                System.out.println("Existing and Valid but event marked as Under Liquidation in ITD database");
                                break;
                            case "ES":
                                displayMessageWithDetails(results.getMessage(),results);
                                System.out.println("Existing and Valid but event marked as Split in ITD database");
                                break;
                            case "EP":
                                displayMessageWithDetails(results.getMessage(),results);
                                System.out.println("Existing and Valid but event marked as Partition in ITD database");
                                break;
                            case "EM":
                                displayMessageWithDetails(results.getMessage(),results);
                                System.out.println("Existing and Valid but event marked as Merger in ITD database");
                                break;

                            case "EL":
                                displayMessageWithDetails(results.getMessage(),results);
                                System.out.println("Existing and Valid but event marked as Liquidated in ITD database");
                                break;
                            case "EI":
                                displayMessageWithDetails(results.getMessage(),results);
                                System.out.println("Existing and Valid but event marked as Dissolution in ITD database");
                                break;
                            case "ED":
                                displayMessageWithDetails(results.getMessage(),results);
                                System.out.println("Existing and Valid but event marked as Death in ITD database");
                                break;
                            case "EA":
                                displayMessageWithDetails(results.getMessage(),results);
                                System.out.println("Existing and Valid but event marked as Amalgamation in ITD database");
                                break;

                            case "EC":
                                displayMessageWithDetails(results.getMessage(),results);
                                System.out.println("Existing and Valid but event marked as Acquisition in ITD database");
                                break;

                            //valid user

                            case "E":
                                displayUserPanDetails(results);
                                System.out.println("Existing and Valid");
                                break;


                        }

                    } else {
                        iconLayout.setVisibility(View.GONE);
                        actionLayout.setVisibility(View.GONE);
                        panStatusLayout.setVisibility(View.VISIBLE);
                        panStatus.setText(results.getMessage());
                        Toast.makeText(PanCardVerificationAcctivity.this, results.getMessage(), Toast.LENGTH_SHORT).show();
                    }

                }else {
                    tryAgain();
                }
            }

            @Override
            public void onFailure(Call<PanCardResponse> call, Throwable t) {
                // Log error here since request failed
                progressDialog.dismiss();
                tryAgain();
                Log.e("Retrofit error", t.toString());
            }
        });
    }

    private void displayUserPanDetails(PanCardResponse results) {

        iconLayout.setVisibility(View.VISIBLE);
        success.setVisibility(View.VISIBLE);
        failed.setVisibility(View.GONE);
        actionLayout.setVisibility(View.GONE);
        userName.setText(results.getPAN().getFirst_name() + " " + results.getPAN().getLast_name());
        userpanNo.setText(results.getPAN().getPan_no());
        userUpdatedOn.setText(results.getPAN().getLast_update());
        panStatus.setText(results.getMessage());

    }

    private void displayMessageWithCross(String message) {

        iconLayout.setVisibility(View.VISIBLE);
        success.setVisibility(View.GONE);
        failed.setVisibility(View.VISIBLE);
        actionLayout.setVisibility(View.GONE);
        panStatus.setText(message);

    }

    private void displayMessageWithDetails(String message,PanCardResponse results) {

        iconLayout.setVisibility(View.GONE);
        actionLayout.setVisibility(View.GONE);
        panStatus.setText(message);

        userName.setText(results.getPAN().getFirst_name() + " " + results.getPAN().getLast_name());
        userpanNo.setText(results.getPAN().getPan_no());
        userUpdatedOn.setText(results.getPAN().getLast_update());
    }

    private void initializeProgress() {

        progressDialog = new ProgressDialog(this);
        progressDialog.show();
        progressDialog.setContentView(R.layout.custom_progressdialog);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCancelable(false);

    }


    private void tryAgain() {
        if (counter == 1||counter==0) {
            counter++;
            inputPancardLayout.setVisibility(View.GONE);
            panStatusLayout.setVisibility(View.GONE);
            detailPanCardLayout.setVisibility(View.VISIBLE);
            actionLayout.setVisibility(View.VISIBLE);
            iconLayout.setVisibility(View.GONE);
            Toast.makeText(PanCardVerificationAcctivity.this, "Try Again !!", Toast.LENGTH_LONG).show();
        } else {
            startActivity(new Intent(PanCardVerificationAcctivity.this, MainActivity.class));
            finish();
        }
    }

}
