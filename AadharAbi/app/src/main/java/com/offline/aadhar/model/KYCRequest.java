package com.offline.aadhar.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class KYCRequest {
    @Expose
    @SerializedName("token")
    private int token;
    @Expose
    @SerializedName("file")
    private String file;

    @Expose
    @SerializedName("image")
    private String image;


    public int getToken() {
        return token;
    }

    public void setToken(int token) {
        this.token = token;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
}
