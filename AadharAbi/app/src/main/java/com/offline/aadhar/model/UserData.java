package com.offline.aadhar.model;

public  class UserData {

    @com.google.gson.annotations.Expose
    @com.google.gson.annotations.SerializedName("OfflinePaperlessKyc")
    private OfflinePaperlessKyc OfflinePaperlessKyc;

    public OfflinePaperlessKyc getOfflinePaperlessKyc() {
        return OfflinePaperlessKyc;
    }

    public void setOfflinePaperlessKyc(OfflinePaperlessKyc OfflinePaperlessKyc) {
        this.OfflinePaperlessKyc = OfflinePaperlessKyc;
    }

    public static class OfflinePaperlessKyc {
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("UidData")
        private UidData UidData;
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("Signature")
        private Signature Signature;

        public UidData getUidData() {
            return UidData;
        }

        public void setUidData(UidData UidData) {
            this.UidData = UidData;
        }

        public Signature getSignature() {
            return Signature;
        }

        public void setSignature(Signature Signature) {
            this.Signature = Signature;
        }
    }

    public static class UidData {
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("Pht")
        private String Pht;
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("Poi")
        private Poi Poi;
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("Poa")
        private Poa Poa;

        public String getPht() {
            return Pht;
        }

        public void setPht(String Pht) {
            this.Pht = Pht;
        }

        public Poi getPoi() {
            return Poi;
        }

        public void setPoi(Poi Poi) {
            this.Poi = Poi;
        }

        public Poa getPoa() {
            return Poa;
        }

        public void setPoa(Poa Poa) {
            this.Poa = Poa;
        }
    }

    public static class Poi {
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("m")
        private String m;
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("name")
        private String name;
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("dob")
        private String dob;
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("e")
        private String e;
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("gender")
        private String gender;

        public String getM() {
            return m;
        }

        public void setM(String m) {
            this.m = m;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getDob() {
            return dob;
        }

        public void setDob(String dob) {
            this.dob = dob;
        }

        public String getE() {
            return e;
        }

        public void setE(String e) {
            this.e = e;
        }

        public String getGender() {
            return gender;
        }

        public void setGender(String gender) {
            this.gender = gender;
        }
    }

    public static class Poa {
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("po")
        private String po;
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("house")
        private String house;
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("landmark")
        private String landmark;
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("state")
        private String state;
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("dist")
        private String dist;
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("street")
        private String street;
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("vtc")
        private String vtc;
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("careof")
        private String careof;
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("pc")
        private String pc;
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("subdist")
        private String subdist;
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("loc")
        private String loc;
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("country")
        private String country;

        public String getPo() {
            return po;
        }

        public void setPo(String po) {
            this.po = po;
        }

        public String getHouse() {
            return house;
        }

        public void setHouse(String house) {
            this.house = house;
        }

        public String getLandmark() {
            return landmark;
        }

        public void setLandmark(String landmark) {
            this.landmark = landmark;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getDist() {
            return dist;
        }

        public void setDist(String dist) {
            this.dist = dist;
        }

        public String getStreet() {
            return street;
        }

        public void setStreet(String street) {
            this.street = street;
        }

        public String getVtc() {
            return vtc;
        }

        public void setVtc(String vtc) {
            this.vtc = vtc;
        }

        public String getCareof() {
            return careof;
        }

        public void setCareof(String careof) {
            this.careof = careof;
        }

        public String getPc() {
            return pc;
        }

        public void setPc(String pc) {
            this.pc = pc;
        }

        public String getSubdist() {
            return subdist;
        }

        public void setSubdist(String subdist) {
            this.subdist = subdist;
        }

        public String getLoc() {
            return loc;
        }

        public void setLoc(String loc) {
            this.loc = loc;
        }

        public String getCountry() {
            return country;
        }

        public void setCountry(String country) {
            this.country = country;
        }
    }

    public static class Signature {
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("SignedInfo")
        private SignedInfo SignedInfo;
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("SignatureValue")
        private String SignatureValue;
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("xmlns")
        private String xmlns;

        public SignedInfo getSignedInfo() {
            return SignedInfo;
        }

        public void setSignedInfo(SignedInfo SignedInfo) {
            this.SignedInfo = SignedInfo;
        }

        public String getSignatureValue() {
            return SignatureValue;
        }

        public void setSignatureValue(String SignatureValue) {
            this.SignatureValue = SignatureValue;
        }

        public String getXmlns() {
            return xmlns;
        }

        public void setXmlns(String xmlns) {
            this.xmlns = xmlns;
        }
    }

    public static class SignedInfo {
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("SignatureMethod")
        private SignatureMethod SignatureMethod;
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("CanonicalizationMethod")
        private CanonicalizationMethod CanonicalizationMethod;
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("Reference")
        private Reference Reference;

        public SignatureMethod getSignatureMethod() {
            return SignatureMethod;
        }

        public void setSignatureMethod(SignatureMethod SignatureMethod) {
            this.SignatureMethod = SignatureMethod;
        }

        public CanonicalizationMethod getCanonicalizationMethod() {
            return CanonicalizationMethod;
        }

        public void setCanonicalizationMethod(CanonicalizationMethod CanonicalizationMethod) {
            this.CanonicalizationMethod = CanonicalizationMethod;
        }

        public Reference getReference() {
            return Reference;
        }

        public void setReference(Reference Reference) {
            this.Reference = Reference;
        }
    }

    public static class SignatureMethod {
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("Algorithm")
        private String Algorithm;

        public String getAlgorithm() {
            return Algorithm;
        }

        public void setAlgorithm(String Algorithm) {
            this.Algorithm = Algorithm;
        }
    }

    public static class CanonicalizationMethod {
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("Algorithm")
        private String Algorithm;

        public String getAlgorithm() {
            return Algorithm;
        }

        public void setAlgorithm(String Algorithm) {
            this.Algorithm = Algorithm;
        }
    }

    public static class Reference {
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("URI")
        private String URI;
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("DigestValue")
        private String DigestValue;
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("DigestMethod")
        private DigestMethod DigestMethod;
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("Transforms")
        private Transforms Transforms;

        public String getURI() {
            return URI;
        }

        public void setURI(String URI) {
            this.URI = URI;
        }

        public String getDigestValue() {
            return DigestValue;
        }

        public void setDigestValue(String DigestValue) {
            this.DigestValue = DigestValue;
        }

        public DigestMethod getDigestMethod() {
            return DigestMethod;
        }

        public void setDigestMethod(DigestMethod DigestMethod) {
            this.DigestMethod = DigestMethod;
        }

        public Transforms getTransforms() {
            return Transforms;
        }

        public void setTransforms(Transforms Transforms) {
            this.Transforms = Transforms;
        }
    }

    public static class DigestMethod {
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("Algorithm")
        private String Algorithm;

        public String getAlgorithm() {
            return Algorithm;
        }

        public void setAlgorithm(String Algorithm) {
            this.Algorithm = Algorithm;
        }
    }

    public static class Transforms {
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("Transform")
        private Transform Transform;

        public Transform getTransform() {
            return Transform;
        }

        public void setTransform(Transform Transform) {
            this.Transform = Transform;
        }
    }

    public static class Transform {
        @com.google.gson.annotations.Expose
        @com.google.gson.annotations.SerializedName("Algorithm")
        private String Algorithm;

        public String getAlgorithm() {
            return Algorithm;
        }

        public void setAlgorithm(String Algorithm) {
            this.Algorithm = Algorithm;
        }
    }
}
