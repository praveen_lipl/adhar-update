package com.offline.aadhar.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ImageREsponse {

    @Expose
    @SerializedName("status_response")
    private String status_response;
    @Expose
    @SerializedName("status")
    private String status;

    public String getStatus_response() {
        return status_response;
    }

    public void setStatus_response(String status_response) {
        this.status_response = status_response;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }
}
