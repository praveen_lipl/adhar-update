package com.offline.aadhar.camera;

/**
 * Created by muralikrishna on 02/11/17.
 */

import android.content.Context;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Point;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.hardware.Camera;
import android.os.Build;
import android.os.Debug;
import android.os.Environment;
import android.util.Log;
import android.widget.Toast;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Random;

import static com.offline.aadhar.camera.CameraActivity.thisActivity;


/**
 * This class has various static utility methods.
 */

public class Util {
    private static final String TAG = Util.class.getSimpleName();
    private static final boolean TORCH_BLACK_LISTED = (Build.MODEL.equals("DROID2"));

    public static final String PUBLIC_LOG_TAG = "card.io";

    private static Boolean sHardwareSupported;

    public static boolean deviceSupportsTorch(Context context) {
        return !TORCH_BLACK_LISTED
                && context.getPackageManager()
                .hasSystemFeature(PackageManager.FEATURE_CAMERA_FLASH);
    }

    @SuppressWarnings("rawtypes")
    public static String manifestHasConfigChange(ResolveInfo resolveInfo, Class activityClass) {
        String error = null;
        if (resolveInfo == null) {
            error = String.format("Didn't find %s in the AndroidManifest.xml",
                    activityClass.getName());
        } else if (!Util.hasConfigFlag(resolveInfo.activityInfo.configChanges,
                ActivityInfo.CONFIG_ORIENTATION)) {
            error = activityClass.getName()
                    + " requires attribute android:configChanges=\"orientation\"";
        }
        if (error != null) {
            Log.e(Util.PUBLIC_LOG_TAG, error);
        }
        return error;
    }

    public static boolean hasConfigFlag(int config, int configFlag) {
        return ((config & configFlag) == configFlag);
    }

    /* --- HARDWARE SUPPORT --- */

    public static boolean hardwareSupported() {
        if (sHardwareSupported == null) {
            sHardwareSupported = hardwareSupportCheck();
        }
        return sHardwareSupported;
    }

    private static boolean hardwareSupportCheck() {
        // Camera needs to open
        Camera c = null;
        try {
            c = Camera.open();
        } catch (RuntimeException e) {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                return true;
            } else {
                Log.w(PUBLIC_LOG_TAG, "- Error opening camera: " + e);
            }
        }
        if (c == null) {
            Log.w(PUBLIC_LOG_TAG, "- No camera found");
            return false;
        } else {
            List<Camera.Size> list = c.getParameters().getSupportedPreviewSizes();
            c.release();

            boolean supportsVGA = false;

            for (Camera.Size s : list) {
                if (s.width == 640 && s.height == 480) {
                    supportsVGA = true;
                    break;
                }
            }

            if (!supportsVGA) {
                Log.w(PUBLIC_LOG_TAG, "- Camera resolution is insufficient");
                return false;
            }
        }
        return true;
    }

    public static String getNativeMemoryStats() {
        return "(free/alloc'd/total)" + Debug.getNativeHeapFreeSize() + "/"
                + Debug.getNativeHeapAllocatedSize() + "/" + Debug.getNativeHeapSize();
    }

    public static void logNativeMemoryStats() {
        Log.d("MEMORY", "Native memory stats: " + getNativeMemoryStats());
    }

    static public Rect rectGivenCenter(Point center, int width, int height) {
        return new Rect(center.x - width / 2, center.y - height / 2, center.x + width / 2, center.y
                + height / 2);
    }

    static public void setupTextPaintStyle(Paint paint) {
        paint.setColor(Color.WHITE);
        paint.setStyle(Paint.Style.FILL);
        paint.setTypeface(Typeface.create(Typeface.SANS_SERIF, Typeface.BOLD));
        paint.setAntiAlias(true);
        float[] black = { 0f, 0f, 0f };
        paint.setShadowLayer(1.5f, 0.5f, 0f, Color.HSVToColor(200, black));
    }


    static void writeCapturedCardImageIfNecessary(
            Intent origIntent, Intent dataIntent, OverlayView mOverlay){
        if (origIntent.getBooleanExtra(CameraActivity.EXTRA_RETURN_CARD_IMAGE, false)
                && mOverlay != null && mOverlay.getBitmap() != null) {
            ByteArrayOutputStream scaledCardBytes = new ByteArrayOutputStream();
            mOverlay.getBitmap().compress(Bitmap.CompressFormat.JPEG, 80, scaledCardBytes);
            dataIntent.putExtra(CameraActivity.EXTRA_CAPTURED_CARD_IMAGE, scaledCardBytes.toByteArray());
        }

    }

    public static File savebitmap(Bitmap bmp, String xmlfilePath,String zipFilePath) {
        String extStorageDirectory = Environment.getExternalStorageDirectory().toString();
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

        OutputStream outStream = null;
        // String temp = null;
        File file = new File(extStorageDirectory, "aadhar_click.png");
        /*if (file.exists()) {
            file.delete();
            file = new File(extStorageDirectory, "aadhar_click.png");
        }*/

        if (file.exists()) file.delete();
        try {
            outStream = new FileOutputStream(file);
            bmp.compress(Bitmap.CompressFormat.JPEG, 90, outStream);
           // bmp.compress(Bitmap.CompressFormat.PNG, 100, outStream);
            outStream.flush();
            outStream.close();

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        Toast.makeText(thisActivity, "Capture Image", Toast.LENGTH_SHORT).show();
        Intent intent=new Intent(thisActivity,ImageActivity.class);
         intent.putExtra("imagePath",file.getAbsolutePath());
         intent.putExtra("zipFilePath",zipFilePath);
         intent.putExtra("xmlfilePath",xmlfilePath);
        thisActivity.startActivity(intent);
        thisActivity.finish();
        Log.e("capture successfuly",file.getAbsolutePath());
        return file;
    }

    public static int generateToken() {
        Random r = new Random();
        return r.nextInt(99999-10)+65;

    }
}

