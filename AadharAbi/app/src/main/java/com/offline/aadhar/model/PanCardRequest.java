package com.offline.aadhar.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public  class PanCardRequest {

    @Expose
    @SerializedName("pan")
    private String   pan;

    @Expose
    @SerializedName("token")
    private int token;

    public String getPan() {
        return pan;
    }

    public void setPan(String pan) {
        this.pan = pan;
    }

    public int getToken() {
        return token;
    }

    public void setToken(int token) {
        this.token = token;
    }
}
