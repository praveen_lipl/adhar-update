package com.offline.aadhar;
import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.BroadcastReceiver;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.provider.Settings;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.karumi.dexter.Dexter;
import com.karumi.dexter.MultiplePermissionsReport;
import com.karumi.dexter.PermissionToken;
import com.karumi.dexter.listener.DexterError;
import com.karumi.dexter.listener.PermissionRequest;
import com.karumi.dexter.listener.PermissionRequestErrorListener;
import com.karumi.dexter.listener.multi.MultiplePermissionsListener;
import com.offline.aadhar.Retrofit.ApiClient;
import com.offline.aadhar.Retrofit.ApiInterface;
import com.offline.aadhar.camera.CameraActivity;
import com.offline.aadhar.model.AadharResponse;
import com.offline.aadhar.utility.MySharedPrefrence;
import com.offline.aadhar.utility.Utils;
import net.lingala.zip4j.core.ZipFile;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.offline.aadhar.Retrofit.ApiClient.URL_KEY;
import static com.offline.aadhar.utility.Utils.checkWriteExternalPermission;
import static com.offline.aadhar.utility.Utils.hideKeyboard;
import static com.offline.aadhar.utility.Utils.isMediaAvailable;
import static com.offline.aadhar.utility.Utils.isNetworkConnected;


public class MainActivity extends AppCompatActivity {

    @BindView(R.id.go)
    Button go;
    @BindView(R.id.panCard)
    Button panCard;
    private ArrayList<String> allZipFileNames;
    private BroadcastReceiver onComplete;
    String finalFile = null, mRootPath;
    public EditText password;
    private boolean isOpenBrowser = false;
    private ProgressDialog progressDialog = null;
    public Dialog dialog;
    MainActivity activity;
    private Runnable runnable = new Runnable() {
        @Override
        public void run() {
            handler.postDelayed(runnable, 2000);
            if (checkFileExist()) {
                handler.removeCallbacks(runnable);
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Log.e("rituraj ", "thread call");
                        startActivity(new Intent(activity, MainActivity.class));
                        finish();
                    }
                }, 4000);
            }
        }

    };
    private Handler handler = new Handler();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());*/
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);

        getThirdPartyUrl();

        dialog = new Dialog(this);
        this.activity = MainActivity.this;



        if (checkWriteExternalPermission(this)){
            if (checkFileExist())
                showDialog("Please Enter your sharecode        " + "  ");
        }else {
            requestStoragePermission();
        }

    }


    public void getThirdPartyUrl() {
        initializeProgress();
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        apiService.getThirdParyUrl().enqueue(new Callback<AadharResponse>() {
            @Override
            public void onResponse(Call<AadharResponse> call, Response<AadharResponse> response) {

                if (response.isSuccessful()) {
                    Log.e("response ",response.body().toString());
                    progressDialog.dismiss();
                    if (response.body() != null && response.body().isStutus()) {
                        MySharedPrefrence.setValueInSession(getApplicationContext(),URL_KEY,""+response.body().getResponse());

                        Log.e("Base Url",MySharedPrefrence.getValueFromSession(getApplicationContext(),URL_KEY));
                    }
                }

            }

            @Override
            public void onFailure(Call<AadharResponse> call, Throwable t) {
                progressDialog.dismiss();
            }
        });
    }


    @OnClick({R.id.panCard, R.id.go})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.panCard:
                startActivity(new Intent(this, PanCardVerificationAcctivity.class));
                break;
            case R.id.go:
                if (isNetworkConnected(this))
                    aadharVerification();
                else
                    Toast.makeText(activity, "Please check Internet connection", Toast.LENGTH_SHORT).show();
                break;
        }
    }

    private void aadharVerification() {
        if (checkWriteExternalPermission(this)) {
            final String appPackageName = "com.android.chrome"; // package name of the app
            Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://resident.uidai.gov.in/offline-kyc"));
            intent.setFlags(Intent.FLAG_ACTIVITY_NO_HISTORY);
            intent.setPackage(appPackageName);
            try {
                Log.d("rituaj", "onClick: inTryBrowser");
                startActivity(intent);
                isOpenBrowser = true;
                handler.postDelayed(runnable, 2000);

            } catch (ActivityNotFoundException ex) {
                Log.e("", "onClick: in inCatchBrowser", ex);
                Toast.makeText(this, "PLease install chrome browser", Toast.LENGTH_SHORT).show();
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + appPackageName)));
                } catch (ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + appPackageName)));
                }
            }
        } else {
            requestStoragePermission();
        }
    }


    private boolean checkFileExist() {
        if (!isMediaAvailable()) {
            Log.e("not", "Media Not Available");
        } else {
            String path = Environment.getExternalStorageDirectory().toString() + File.separator + Environment.DIRECTORY_DOWNLOADS;
            String extension, fileName;
            File file = new File(path);
            mRootPath = file.getAbsoluteFile().getPath();
            allZipFileNames = new ArrayList<String>();
            File filesInDirectory[] = file.listFiles();
            if (filesInDirectory != null) {
                for (int i = 0; i < filesInDirectory.length; i++) {
                    fileName = filesInDirectory[i].getName();
                    Log.e("File name", filesInDirectory[i].getName());
                    if (fileName.contains(".")) {
                        extension = fileName.substring(fileName.lastIndexOf("."));
                        if (extension.equalsIgnoreCase(".zip"))
                            allZipFileNames.add(filesInDirectory[i].getName());
                    }
                }
                Log.e("allZipFileNames ", allZipFileNames + "");
            }
        }
        return getFinalZippFile(allZipFileNames);
    }

    private boolean getFinalZippFile(ArrayList<String> allZipFileNames) {
        boolean getFile = false;
        for (int i = 0; i < allZipFileNames.size(); i++) {
            if (allZipFileNames.get(i).contains("offlineaadhaar")) {
                finalFile = allZipFileNames.get(i);
                Log.e("finalFile Name", finalFile);
                getFile = true;
                break;
            }
        }
        return getFile;
    }

    public void showDialog(String msg) {
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setCancelable(false);
        dialog.setContentView(R.layout.sharecode_dailog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        password = (EditText) dialog.findViewById(R.id.inputShareCode);
        final TextView txt_msg = (TextView) dialog.findViewById(R.id.txt_msg);
        txt_msg.setText(msg);
        Button btn_submit = (Button) dialog.findViewById(R.id.btn_submit);
        Button btn_cancel = (Button) dialog.findViewById(R.id.btn_cancel);
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!TextUtils.isEmpty(password.getText().toString())) {
                    hideKeyboard(MainActivity.this);
                    initializeProgress();
                    extractZippFile(finalFile, password.getText().toString());
                    // dialog.dismiss();
                } else {
                    Toast.makeText(MainActivity.this, "Please insert share code", Toast.LENGTH_SHORT).show();
                }

            }
        });
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dialog.dismiss();
                Log.e("finalFile", finalFile);
                Utils.deleteFile(finalFile);
            }
        });
        dialog.show();
    }

    private void initializeProgress() {
        progressDialog = new ProgressDialog(this);
        progressDialog.show();
        progressDialog.setContentView(R.layout.custom_progressdialog);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCancelable(false);

    }


    private void extractZippFile(String finalFile, String shareCode) {
        String unzipLocation = Environment.getExternalStorageDirectory() + "/" + Environment.DIRECTORY_DOWNLOADS + "/"; // destination folder location
        Log.e("zip file", unzipLocation + finalFile);
        Log.e("unzip filelocation", unzipLocation);
        unzip(unzipLocation + finalFile, Environment.getExternalStorageDirectory() + "/" + Environment.DIRECTORY_DOWNLOADS + "/", shareCode);
    }

    public void unzip(String targetZipFilePath, String destinationFolderPath, String sharecode) {
        try {
            ZipFile zipFile = new ZipFile(targetZipFilePath);
            if (zipFile.isEncrypted()) {
                zipFile.setPassword(sharecode);
            }
            zipFile.extractAll(destinationFolderPath);
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    getXML();
                }
            }, 2000);
        } catch (Exception e) {
            e.printStackTrace();
            Utils.deleteXmlFile(targetZipFilePath);
            progressDialog.dismiss();
            password.setError("Enter correct sharecode");
            Toast.makeText(MainActivity.this, "Wrong ShareCode", Toast.LENGTH_LONG).show();
        }
    }

    private void getXML() {
        dialog.dismiss();
        if (!isMediaAvailable()) {
            Log.e("not", "Media Not Available");
        } else {
            String path = Environment.getExternalStorageDirectory().toString() + File.separator + Environment.DIRECTORY_DOWNLOADS;
            String extension, fileName;
            File file = new File(path);
            mRootPath = file.getAbsoluteFile().getPath();
            allZipFileNames = new ArrayList<String>();

            File filesInDirectory[] = file.listFiles();
            if (filesInDirectory != null) {
                for (int i = 0; i < filesInDirectory.length; i++) {
                    fileName = filesInDirectory[i].getName();
                    Log.e("File name", filesInDirectory[i].getName());
                    if (fileName.contains(".")) {
                        extension = fileName.substring(fileName.lastIndexOf("."));
                        if (extension.equalsIgnoreCase(".xml"))
                            allZipFileNames.add(filesInDirectory[i].getName());
                    }
                }
                Log.e("allZipFileNames ", allZipFileNames + "");
                getFinalXMLFile(allZipFileNames);
            }
        }
    }

    private void getFinalXMLFile(ArrayList<String> allZipFileNames) {
        String finalFile = null;
        for (int i = 0; i < allZipFileNames.size(); i++) {
            if (allZipFileNames.get(i).contains("offlineaadhaar")) {
                finalFile = allZipFileNames.get(i);
                Log.e("finalFile Name", finalFile);
                break;
            }
        }
        progressDialog.dismiss();
        String path = Environment.getExternalStorageDirectory().toString() + File.separator + Environment.DIRECTORY_DOWNLOADS + File.separator + finalFile;
        CameraActivity.takenImage = true;
        Intent intent = new Intent(MainActivity.this, CameraActivity.class);
        intent.putExtra("xmlfilePath", path);
        intent.putExtra("zipFilePath", finalFile);
        startActivity(intent);

    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
        if (progressDialog != null)
            progressDialog.dismiss();
    }

    ////////////////////////////// App runtime permission//////////////////////////////////////
    public  void requestStoragePermission() {

        Dexter.withActivity(MainActivity.this)
                .withPermissions(
                        Manifest.permission.READ_EXTERNAL_STORAGE,
                        Manifest.permission.WRITE_EXTERNAL_STORAGE,
                        Manifest.permission.CAMERA)
                .withListener(new MultiplePermissionsListener() {
                    @Override
                    public void onPermissionsChecked(MultiplePermissionsReport report) {
                        // check if all permissions are granted
                        if (report.areAllPermissionsGranted()) {
                            //isPermissionGranted = true;
                            Log.e("all permission granted","granted");
                        }

                        // check for permanent denial of any permission
                        if (report.isAnyPermissionPermanentlyDenied()) {
                            // show alert dialog navigating to Settings
                            showSettingsDialog();
                        }
                    }

                    @Override
                    public void onPermissionRationaleShouldBeShown(List<PermissionRequest> permissions, PermissionToken token) {
                        token.continuePermissionRequest();
                    }
                }).
                withErrorListener(new PermissionRequestErrorListener() {
                    @Override
                    public void onError(DexterError error) {
                        Toast.makeText(activity, "Error occurred! ", Toast.LENGTH_SHORT).show();
                    }
                })
                .onSameThread()
                .check();
    }

    public  void showSettingsDialog( ) {
        AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
        builder.setTitle("Need Permissions");
        builder.setMessage("This app needs permission to use this feature. You can grant them in app settings.");
        builder.setPositiveButton("GOTO SETTINGS", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
                openSettings();
            }
        });
        builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });
        builder.show();

    }
    // navigating user to app settings
    public  void openSettings( ) {
        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS);
        Uri uri = Uri.fromParts("package", getPackageName(), null);
        intent.setData(uri);
        startActivityForResult(intent, 101);
    }

}
