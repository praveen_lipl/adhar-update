package com.offline.aadhar.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AadharRequest {

    @Expose
    @SerializedName("file")
    private String file;
    @Expose
    @SerializedName("token")
    private int token;

    public AadharRequest(int token, String base64) {
        this.token = token;
        this.file = base64;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public int getToken() {
        return token;
    }

    public void setToken(int token) {
        this.token = token;
    }
}
