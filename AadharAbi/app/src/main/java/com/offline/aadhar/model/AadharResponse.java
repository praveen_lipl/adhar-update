package com.offline.aadhar.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AadharResponse {

    @Expose
    @SerializedName("status")
    private boolean stutus;

    @Expose
    @SerializedName("response")
    private String response;


    @Expose
    @SerializedName("aadhaar")
    private Aadhaar aadhaar;

    @Expose
    @SerializedName("token")
    private String token;
    @Expose
    @SerializedName("message")
    private String message;
    @Expose
    @SerializedName("face_status")
    private String face_status;
    @Expose
    @SerializedName("authCheck")
    private String authCheck;

    public Aadhaar getAadhaar() {
        return aadhaar;
    }

    public void setAadhaar(Aadhaar aadhaar) {
        this.aadhaar = aadhaar;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getFace_status() {
        return face_status;
    }

    public void setFace_status(String face_status) {
        this.face_status = face_status;
    }

    public String getAuthCheck() {
        return authCheck;
    }

    public void setAuthCheck(String authCheck) {
        this.authCheck = authCheck;
    }

    public boolean isStutus() {
        return stutus;
    }

    public void setStutus(boolean stutus) {
        this.stutus = stutus;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }

    public static class Aadhaar {
        @Expose
        @SerializedName("photo")
        private String photo;
        @Expose
        @SerializedName("Name")
        private String Name;
        @Expose
        @SerializedName("Mother_Name")
        private String Mother_Name;
        @Expose
        @SerializedName("Gender")
        private String Gender;
        @Expose
        @SerializedName("DOB")
        private String DOB;
        @Expose
        @SerializedName("Address")
        private String Address;
        @Expose
        @SerializedName("Aadhaar_No")
        private String Aadhaar_No;

        public String getPhoto() {
            return photo;
        }

        public void setPhoto(String photo) {
            this.photo = photo;
        }

        public String getName() {
            return Name;
        }

        public void setName(String Name) {
            this.Name = Name;
        }

        public String getMother_Name() {
            return Mother_Name;
        }

        public void setMother_Name(String Mother_Name) {
            this.Mother_Name = Mother_Name;
        }

        public String getGender() {
            return Gender;
        }

        public void setGender(String Gender) {
            this.Gender = Gender;
        }

        public String getDOB() {
            return DOB;
        }

        public void setDOB(String DOB) {
            this.DOB = DOB;
        }

        public String getAddress() {
            return Address;
        }

        public void setAddress(String Address) {
            this.Address = Address;
        }

        public String getAadhaar_No() {
            return Aadhaar_No;
        }

        public void setAadhaar_No(String Aadhaar_No) {
            this.Aadhaar_No = Aadhaar_No;
        }
    }
}
