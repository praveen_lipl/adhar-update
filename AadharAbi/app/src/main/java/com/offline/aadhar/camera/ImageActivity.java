package com.offline.aadhar.camera;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.bumptech.glide.Glide;
import com.offline.aadhar.MainActivity;
import com.offline.aadhar.R;
import com.offline.aadhar.Retrofit.ApiClient;
import com.offline.aadhar.Retrofit.ApiInterface;
import com.offline.aadhar.model.AadharResponse;
import com.offline.aadhar.model.KYCRequest;
import com.offline.aadhar.utility.MySharedPrefrence;
import com.offline.aadhar.utility.Utils;

import java.io.File;
import java.util.HashMap;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

import static com.offline.aadhar.Retrofit.ApiClient.URL_KEY;
import static com.offline.aadhar.camera.Util.generateToken;
import static com.offline.aadhar.utility.Utils.getBase64FromPath;
import static com.offline.aadhar.utility.Utils.isNetworkConnected;

public class ImageActivity extends AppCompatActivity {
    String filePath, xmlfilePath, zipFilePath;
    @BindView(R.id.userImage)
    ImageView userImage;
    @BindView(R.id.retry)
    Button retry;
    @BindView(R.id.submit)
    Button submit;
    @BindView(R.id.userName)
    TextView userName;
    @BindView(R.id.userAadharNo)
    TextView userAadharNo;
    @BindView(R.id.userDOB)
    TextView userDOB;
    @BindView(R.id.close)
    Button close;
    @BindView(R.id.tryAgain)
    Button tryAgain;
    @BindView(R.id.done)
    Button done;
    @BindView(R.id.retake)
    Button retake;
    @BindView(R.id.success)
    ImageView success;
    @BindView(R.id.failed)
    ImageView failed;
    @BindView(R.id.iconLayout)
    LinearLayout iconLayout;
    @BindView(R.id.tryAgainLayout)
    LinearLayout tryAgainLayout;
    @BindView(R.id.detailAadharLayout)
    LinearLayout detailAadharLayout;
    @BindView(R.id.ImageLayout)
    LinearLayout ImageLayout;
    @BindView(R.id.userImageView)
    ImageView userImageView;
    @BindView(R.id.userCareOf)
    TextView userCareOf;
    @BindView(R.id.userGender)
    TextView userGender;
    @BindView(R.id.userAddress)
    TextView userAddress;
    @BindView(R.id.aadharStatus)
    TextView aadharStatus;
    @BindView(R.id.aadharStatusLayout)
    LinearLayout aadharStatusLayout;
    private ProgressDialog progressDialog;
    int counter = 0;
    public String retakeStatus = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
        StrictMode.setVmPolicy(builder.build());*/
        setContentView(R.layout.activity_image);
        ButterKnife.bind(this);
        if (getIntent().getExtras() != null) {
            filePath = getIntent().getExtras().getString("imagePath");
            xmlfilePath = getIntent().getExtras().getString("xmlfilePath");
            zipFilePath = getIntent().getExtras().getString("zipFilePath");
            Log.e("user ImagePath", filePath);
            Log.e("user xmlfilePath", xmlfilePath);
            userImage.setImageURI(Uri.fromFile(new File(filePath)));
        }

    }

    private void initializeProgress() {
        progressDialog = new ProgressDialog(this);
        progressDialog.show();
        progressDialog.setContentView(R.layout.custom_progressdialog);
        progressDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        progressDialog.setCancelable(false);
    }

    @OnClick({R.id.retry, R.id.submit, R.id.close, R.id.tryAgain, R.id.success, R.id.retake, R.id.done})
    public void onViewClicked(View view) {
        switch (view.getId()) {
            case R.id.retry:
                CameraActivity.takenImage = true;
                startActivity(new Intent(ImageActivity.this, CameraActivity.class));
                finish();
                break;
            case R.id.submit:
                sendXmltoServer();
                break;
            case R.id.retake:
                CameraActivity.takenImage = true;
                startActivity(new Intent(ImageActivity.this, CameraActivity.class));
                finish();
                break;

            case R.id.close:
                startActivity(new Intent(ImageActivity.this, MainActivity.class));
                finish();
                break;
            case R.id.tryAgain:
                sendXmltoServer();
                break;
            case R.id.done:
                File file = new File(zipFilePath);
                String strFileName = file.getName().replaceFirst("[.][^.]+$", "");
                Utils.deleteFile(strFileName + ".zip");
                Utils.deleteXmlFile(xmlfilePath);
                Utils.deleteFile(filePath);
                startActivity(new Intent(ImageActivity.this, MainActivity.class));
                finish();
                break;

        }
    }

    private void sendXmltoServer() {
        if (filePath != null && xmlfilePath != null)
            if (isNetworkConnected(this))
                if (!MySharedPrefrence.getValueFromSession(this,URL_KEY).equalsIgnoreCase(""))
                    callApi();
                else Toast.makeText(ImageActivity.this, "Base URL not found", Toast.LENGTH_SHORT).show();
            else
                Toast.makeText(ImageActivity.this, "Please check Internet connection", Toast.LENGTH_SHORT).show();
        else Toast.makeText(this, "Something went wrong", Toast.LENGTH_LONG).show();
    }

    private void callApi() {
        int UserToken = generateToken();
        initializeProgress();
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        KYCRequest aadharResponse = new KYCRequest();
        aadharResponse.setImage(getBase64FromPath(filePath));
        aadharResponse.setFile(getBase64FromPath(xmlfilePath));
        aadharResponse.setToken(UserToken);
       /* HashMap hashMap = new HashMap();
        hashMap.put("Content-Type", "application/json;charset=UTF-8");*/
        Call<AadharResponse> call = apiService.sendImagetoServer(MySharedPrefrence.getValueFromSession(getApplicationContext(),URL_KEY)+"aadhaarws/aadhaarws/offlineekycval",aadharResponse);
         Log.e("Adhar URL",MySharedPrefrence.getValueFromSession(getApplicationContext(),URL_KEY)+"aadhaarws/aadhaarws/offlineekycval");
        call.enqueue(new Callback<AadharResponse>() {
            @Override
            public void onResponse(Call<AadharResponse> call, Response<AadharResponse> response) {
                progressDialog.dismiss();
                detailAadharLayout.setVisibility(View.VISIBLE);
                ImageLayout.setVisibility(View.GONE);
                if (response.isSuccessful()) {
                    AadharResponse results = response.body();

                    if (UserToken == Integer.parseInt(results.getToken())) {
                        Toast.makeText(ImageActivity.this, results.getMessage(), Toast.LENGTH_LONG).show();

                        Log.d("Retrofit token", "result from server: " + results.getToken());
                        try {

                            if (results.getAadhaar() != null && results.getAuthCheck().equalsIgnoreCase("true")) {
                                if (results.getFace_status().equalsIgnoreCase("NA")) {
                                    retakeStatus = results.getFace_status();
                                    displayNotMatchedRetry(results, true);
                                    System.out.println("Couldnot process face match. Kindly retake Photo.");
                                } else {
                                    File file = new File(zipFilePath);
                                    String strFileName = file.getName().replaceFirst("[.][^.]+$", "");
                                    Utils.deleteFile(strFileName + ".zip");
                                    Utils.deleteXmlFile(xmlfilePath);
                                    Utils.deleteFile(filePath);

                                    switch (results.getFace_status()) {
                                        //
                                        case "M":
                                            //success
                                            displayMessageWithData(results);
                                            System.out.println("KYC completed sucessfully.");
                                            break;
                                        case "NM":
                                            displayNotMatchedRetry(results, false);
                                            System.out.println("Photo doesn't match, KYC Incomplete.");
                                            break;

                                    }
                                }

                            } else if (results.getAuthCheck().equalsIgnoreCase("false")) {
                                displayMessageWithCross(results.getMessage());
                            }

                        } catch (Exception e) {
                            e.printStackTrace();
                        }

                    } else {
                        tryAgain();
                        Toast.makeText(ImageActivity.this, "Token Not Matched", Toast.LENGTH_LONG).show();

                    }

                } else {

                    tryAgain();
                }

            }

            @Override
            public void onFailure(Call<AadharResponse> call, Throwable t) {
                // Log error here since request failed
                progressDialog.dismiss();

                tryAgain();
                Log.e("Retrofit error", t.toString());
            }
        });
    }

    private void displayNotMatchedRetry(AadharResponse results, boolean isRetry) {

        tryAgainLayout.setVisibility(View.GONE);
        userName.setText(results.getAadhaar().getName());
        userAadharNo.setText(results.getAadhaar().getAadhaar_No());
        userDOB.setText(results.getAadhaar().getDOB());

        userAddress.setText(results.getAadhaar().getAddress());
        userCareOf.setText(results.getAadhaar().getMother_Name());
        userGender.setText(results.getAadhaar().getGender());
        Glide.with(ImageActivity.this)
                .load(Base64.decode(results.getAadhaar().getPhoto(), Base64.DEFAULT))
                .placeholder(R.drawable.user)
                .into(userImageView);

        aadharStatus.setText(results.getMessage());
        iconLayout.setVisibility(View.VISIBLE);
        success.setVisibility(View.GONE);
        failed.setVisibility(View.VISIBLE);

        if (isRetry) {
            retake.setVisibility(View.VISIBLE);
            done.setVisibility(View.GONE);

        } else {
            done.setVisibility(View.VISIBLE);
        }

    }


    private void displayMessageWithCross(String message) {
        tryAgainLayout.setVisibility(View.GONE);
        iconLayout.setVisibility(View.VISIBLE);
        failed.setVisibility(View.VISIBLE);
        success.setVisibility(View.GONE);
        aadharStatus.setText(message);
        done.setVisibility(View.VISIBLE);

    }

    private void displayMessageWithData(AadharResponse results) {
        aadharStatus.setText(results.getMessage());
        tryAgainLayout.setVisibility(View.GONE);
        userName.setText(results.getAadhaar().getName());
        userAadharNo.setText(results.getAadhaar().getAadhaar_No());
        userDOB.setText(results.getAadhaar().getDOB());
        done.setVisibility(View.VISIBLE);
        userAddress.setText(results.getAadhaar().getAddress());
        userCareOf.setText(results.getAadhaar().getMother_Name());
        userGender.setText(results.getAadhaar().getGender());
        Glide.with(ImageActivity.this)
                .load(Base64.decode(results.getAadhaar().getPhoto(), Base64.DEFAULT))
                .placeholder(R.drawable.user)
                .into(userImageView);


        iconLayout.setVisibility(View.VISIBLE);
        if (results.getAuthCheck().equalsIgnoreCase("true")) {
            success.setVisibility(View.VISIBLE);
            failed.setVisibility(View.GONE);
        } else {
            success.setVisibility(View.GONE);
            failed.setVisibility(View.VISIBLE);
        }


    }

    private void tryAgain() {

        if (counter == 1 || counter == 0) {
            counter++;
            detailAadharLayout.setVisibility(View.VISIBLE);
            ImageLayout.setVisibility(View.GONE);
            done.setVisibility(View.GONE);
            iconLayout.setVisibility(View.GONE);
            tryAgainLayout.setVisibility(View.VISIBLE);
            Toast.makeText(ImageActivity.this, "Try Again !!", Toast.LENGTH_LONG).show();
        } else {
            startActivity(new Intent(ImageActivity.this, MainActivity.class));
            finish();
        }
    }


}
