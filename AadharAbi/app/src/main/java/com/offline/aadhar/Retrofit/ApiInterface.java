package com.offline.aadhar.Retrofit;

import com.offline.aadhar.model.AadharRequest;
import com.offline.aadhar.model.AadharResponse;
import com.offline.aadhar.model.KYCRequest;
import com.offline.aadhar.model.PanCardRequest;
import com.offline.aadhar.model.PanCardResponse;

import java.util.HashMap;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.GET;
import retrofit2.http.HeaderMap;
import retrofit2.http.POST;
import retrofit2.http.Url;

public interface ApiInterface {

    @GET("getAadharSettings")
    Call<AadharResponse> getThirdParyUrl();


    @POST
    Call<AadharResponse> sendImagetoServer(@Url String url, @Body KYCRequest aadharRequest);

    @POST
    Call<PanCardResponse> sendPanCardNumber(@Url String url,@Body PanCardRequest panRequest);
}